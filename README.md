# 1. diagram
![](diagram.png)
![](diagram2.png)
# 2. update_whois_rdns -- ETL
## 1.1 whois:
- Extract:

    ./run download

- Transform:

    ./run text2db
    ./run db2csv

- Load:

    cat *.csv >whois.csv

## 1.2 rdns:
- Extract:

    download latest rdns.gz from [censys.io]()

- Transform & Load:

    ./run import_rdns

# 3 .labeling -- IP address labeling (Upstream):
- integrate the following into production system:
  - links_server_whois.py
  - markToolWhois.py
  - import_whois.sh
  - bulk_whois.sql

# 4. cleansing -- cleansing edge_table (Downstream, one off):


    ./run