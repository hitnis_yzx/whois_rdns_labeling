import networkx as nx

p = None
g = nx.Graph()
pop_id = 1

def output():
  global pop_id
  cc_l = sorted(nx.connected_components(g), key=lambda x:len(x), reverse=True)
  i = 0
  for i in range(len(cc_l)):
    cc = cc_l[i]
    if len(cc) <= 10:
      break
    for n in cc:
      print n + ',' + str(pop_id)
    pop_id += 1
  for cc in cc_l[i:]:
    for c in cc:
      print c + ',' + str(pop_id)
  pop_id += 1

while True:
  try:
    l = raw_input().strip()
  except:
    break
  f = l.split('|')
  g.add_edge(f[2],f[3])
  if p and not f[:2] == p:
    output()
    g = nx.Graph()
  p = f[:2]

if p:
  output()
