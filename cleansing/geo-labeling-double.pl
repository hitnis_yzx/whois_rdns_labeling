#!/usr/local/bin/perl

use strict;

my $geolite_block_file = $ARGV[0] ? $ARGV[0] : "-";

use Net::Patricia;
my $block_tb = new Net::Patricia;

&load_geolite_country();

while(<STDIN>) {
  chomp;
  my @s = split /\t/, $_;
  print &get_geo($s[0]) . "|" . &get_geo($s[1]) . "|" . join("|", @s) . "\n";
}

sub get_geo {
  my $ip_addr = shift;
  my $ret = $block_tb->match_string($ip_addr);
  return $ret ?  $ret : "-";
}

sub load_geolite_country {
  open my $fh, "<", $geolite_block_file or die "Can not open file: $!\n";
  while(<$fh>){
    chomp;
    my @F = split (/,/, $_, 2);
    $block_tb->add_string($F[0], $F[1]);
  }
  close $fh;
}
