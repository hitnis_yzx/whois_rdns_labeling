export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
mysql_ip=10.10.11.150
mysql_username=root
mysql_password=1q2w3e4r
dbname=edges
edge_tblname=_ipv4_test_whois8_edge_table
node_tblname=_ipv4_test_whois8_node_table

export_edges_statement(){
  db=$1
  tbl=$2
  cat << "EOF" | sed "s/<db>/$1/" | sed "s/<tbl>/$2/"
use <db>;
SELECT in_ip,out_ip INTO OUTFILE '/var/lib/mysql-files/edges.csv'
  FIELDS TERMINATED BY ','
  LINES TERMINATED BY '\n'
  FROM <tbl>;
EOF
}
export -f export_edges_statement

import_statement(){
  db=$1
  tbl=$2
  cat << "EOF" | sed "s/<db>/$1/" | sed "s/<tbl>/$2/"
use <db>;
drop table if exists pop_nodes;
create table if not exists pop_nodes(
  ip varchar(64),
  pop_id int(11),
  primary key (ip),
  key ip_index (ip)
) engine InnoDB;
load data local infile 'pop-nodes.csv' INTO TABLE pop_nodes FIELDS TERMINATED BY ',';

UPDATE <tbl> AS A
LEFT OUTER JOIN pop_nodes AS B
ON A.ip=B.ip
SET A.pop_id=B.pop_id;

drop table if exists pop_nodes;
EOF
}
export -f import_statement

export_edges(){
  expect -c " \
    set timeout -1
    log_user 1
    spawn bash -c \"mysql -h $mysql_ip -u $mysql_username -p < <(export_edges_statement $dbname $edge_tblname)\"
    expect -re \".*password.*\" {send \"$mysql_password\r\n\"}
    expect eof \
  "
}

pop_labeling(){
  # csv header
  echo 'ip,pop_id' >pop-nodes.csv

  # labeled links: in_geo, out_geo, in_ip, out_ip
  #   =[in_geo eq out_geo]=> intra-city-labeled.csv (ip,geo,cc_id)
  #                       |=> geo2ncc.csv (geo,ncc)
  #   =[in_geo ne out_geo]=> inter-city.csv (ip,geo)

  # debug
  # cat test_edges.csv | perl geo-labeling-double.pl cidr2geo.csv | \
  # cat test.geo | \

  cat /var/lib/mysql-files/edges.csv | perl geo-labeling-double.pl cidr2geo.csv | \
    tee \
      >(cat | awk -F'|' '$1==$2' | sort -k1,1 | python calc_cc.py 1> >(cat | sort -t'|' -k1,2 -u >intra-city-labeled.csv) 2> >(cat | sort -t'|' -k1,1 >geo2ncc.csv)) \
      >(cat | awk -F'|' '{if($1!=$2){print $3"|"$1"\n"$4"|"$2}}' | sort -t'|' -k1,2 -u >inter-city.csv) \
      >/dev/null

  sleep 1 # to fix weird synchronization issue with tee

  # singleton inter-city node cc_id labeling
  # (ip,geo,cc_id)
  join -t'|' -1 2 -2 1\
    -o '1.1,1.2,2.2' \
    <( sort -t'|' -k2,2 <(comm -1 -3 <(cut -d'|' -f1,2 intra-city-labeled.csv) <(cut -d'|' -f1,2 inter-city.csv)) ) \
    geo2ncc.csv >inter-city-labeled.csv

  # (geo,cc_id) -> pop_id
  cat intra-city-labeled.csv inter-city-labeled.csv | sort -t'|' -k2,2 -k3,3n | python <(
  cat << "EOF"
p = None
pop_id = 1
while True:
  try:
    l = raw_input().strip()
  except:
    break
  ip, pop = l.split('|', 1)
  print ','.join(str(x) for x in [ ip, pop_id ])
  if p and pop != p:
    pop_id += 1
  p = pop
EOF
  ) >> pop-nodes.csv
}

import(){
  expect -c " \
    set timeout -1
    log_user 1
    spawn bash -c \"mysql -h $mysql_ip -u $mysql_username -p < <(import_statement $dbname $node_tblname);\"
    expect -re \".*password.*\" {send \"$mysql_password\r\n\"}
    expect eof \
  "
}
# pop
export_edges
pop_labeling
# import
