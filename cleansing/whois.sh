mysql_ip=10.10.11.150
mysql_username=root
mysql_password=1q2w3e4r
dbname=edges
# tblname=_ipv4_test_whois8_node_table
tblname=node_table

export_statement(){
  db=$1
  tbl=$2
  cat << "EOF" | sed "s/<db>/$1/" | sed "s/<tbl>/$2/"
USE <db>;
SELECT ip INTO OUTFILE '/var/lib/mysql-files/nodes.csv'
  FIELDS TERMINATED BY ','
  LINES TERMINATED BY '\n'
  FROM <tbl>;
EOF
}
export -f export_statement

import_statement(){
  db=$1
  tbl=$2
  cat << "EOF" | sed "s/<db>/$1/" | sed "s/<tbl>/$2/"
use <db>;
drop table if exists whois_nodes;
create table if not exists whois_nodes(
  ip varchar(64),
  whois varchar(64),
  primary key (ip),
  key ip_index (ip)
) engine InnoDB;
load data local infile 'whois-nodes.csv' INTO TABLE whois_nodes FIELDS TERMINATED BY '\t';

UPDATE <tbl> AS A
LEFT OUTER JOIN whois_nodes AS B
ON A.ip=B.ip
SET A.whois=B.whois;
EOF
}
export -f import_statement

rdns_statement(){
  db=$1
  tbl=$2
  cat << "EOF" | sed "s/<db>/$1/" | sed "s/<tbl>/$2/"
use <db>;
UPDATE <tbl> AS A
LEFT OUTER JOIN rdns AS B
ON A.ip=B.ip
SET A.domain=B.domain;
EOF
}
export -f rdns_statement

export(){
  expect -c " \
    set timeout -1
    log_user 1
    spawn bash -c \"mysql -h $mysql_ip -u $mysql_username -p < <(export_statement $dbname $tblname)\"
    expect -re \".*password.*\" {send \"$mysql_password\r\n\"}
    expect eof \
  "
}

labeling(){
  echo -e "ip\twhois" >whois-nodes.csv
  cat /var/lib/mysql-files/nodes.csv | perl geo-labeling.pl whois.csv >>whois-nodes.csv
}

import(){
  expect -c " \
    set timeout -1
    log_user 1
    spawn bash -c \"mysql -h $mysql_ip -u $mysql_username -p < <(import_statement $dbname $tblname);\"
    expect -re \".*password.*\" {send \"$mysql_password\r\n\"}
    expect eof \
  "
}

rdns(){
  expect -c " \
    set timeout -1
    log_user 1
    spawn bash -c \"mysql -h $mysql_ip -u $mysql_username -p < <(rdns_statement $dbname $tblname);\"
    expect -re \".*password.*\" {send \"$mysql_password\r\n\"}
    expect eof \
  "
}

# # whois
time export
#time labeling
#time import

# # rdns
# rdns
