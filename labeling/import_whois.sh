#!/bin/bash

task_dir=$1
neo4j_file_url=$2
task=$3
mysql_host="10.10.11.150"
neo4j_host="10.10.11.150"
_task=_$task
mysql_pass="1q2w3e4r"
neo4j_pass="root"
expect -c " \
  set timeout -1
  spawn bash -c \"mysql -h $mysql_host -u root -p < <(cat bulk_whois.sql \
    | sed 's/node_table/${_task}_node_table/g' \
    | sed 's/edge_table/${_task}_edge_table/g' \
    | sed 's|nodes.csv|$task_dir/$task/nodes.csv|g' \
    | sed 's|links.csv|$task_dir/$task/links.csv|g')\"
  log_user 1
  expect -re \".*password.*\" {send \"$mysql_pass\r\n\"}
  expect eof \
"

  cypher-shell -a $neo4j_host -u neo4j -p "$neo4j_pass" < <(cat load-csv.cql \
    | sed "s/<node>/${_task}_node_table/g" \
    | sed "s/<edge>/${_task}_edge_table/g" \
    | sed "s|<nodes>|$neo4j_file_url/$task/neo4j-nodes.csv|g" \
    | sed "s|<links>|$neo4j_file_url/$task/neo4j-links.csv|g")
