import json, os, sys, socket, urllib, threading, time
from markToolWhois import *
from settings import *


class Processer:

    def __init__(self):
        if not os.path.isdir(task_list_dir):
            os.mkdir(task_list_dir)
        t=threading.Thread(target=self.startHTTPServerForNeo4j);
        t.start()
        self.rtr_info = loadRTRInfo("rtr.info")
        self.rtree = buildBGPPrefixTree("prefix2as.csv")
        self.whois_trie = buildWhoisTrie("whois.csv")
        self.ip_info_list = loadGEOInfo("iprange2geo.csv")
        # self.osscan2node = loadOSScan("osscan2node.new.nodes")
        print "build processer complete, ready!"

    def startHTTPServerForNeo4j(self):
        print "open http server..."
        neo4j_file_url_port = 11949
        os.system("cd %s;python -m SimpleHTTPServer %d" % (task_list_dir, neo4j_file_url_port))

    def startConvert(self, sock, addr):
        print "start convert"
        command = sock.recv(65536)
        print command
        dic = {}
        try:
            command_info = json.loads(command.strip())
        except:
            dic["errcode"] = 1
            dic["msg"] = "fail to decode json string"
            sock.sendall(bytes(json.dumps(dic)))
            sock.close()
            print "convert fail"
            return
        if "action" in command_info:
            if (command_info["action"] == "links" or command_info["action"] == "ipv6_links" or command_info["action"] == "update_ipv6_links" or command_info["action"] == "update_links") and "task_id" in command_info and "url" in command_info:
                task_id = command_info["task_id"].replace("-", "_").replace(".", "_")
                if command_info["action"] == "ipv6_links" or command_info["action"] == "update_ipv6_links":
                    task_id = "ipv6_" + task_id
                else:
                    task_id = "ipv4_" + task_id
                url = command_info["url"]
                if url != "" and task_id != "":
                    if command_info["action"] == "update_links" or command_info["action"] == "update_ipv6_links":
                        clearOldLinksTask(os.path.dirname(os.path.realpath(__file__))+"/"+task_list_dir, neo4j_import_path, task_id)
                    if not os.path.isdir(task_list_dir+"/"+task_id):
                        os.mkdir(task_list_dir+"/"+task_id)
                        command = "wget -O %s %s" % ("%s/origin.links" % (task_list_dir+"/"+task_id), url)
                        print command
                        if os.system(command) != 0:
                            dic["errcode"] = 1
                            dic["msg"] = "wget %s failed" % url
                        else:
                            ret_dic = processLinksToLabel("%s/origin.links" % (task_list_dir+"/"+task_id), self.rtr_info, self.rtree, self.whois_trie, self.ip_info_list, task_list_dir+"/"+task_id, "node-task-%s" % task_id, "edge-task-%s" % task_id)
                            # linksToDatabase(os.path.dirname(os.path.realpath(__file__))+"/"+task_list_dir, neo4j_import_path, task_id)
                            os.system("./import_whois.sh %s %s %s" % (task_list_dir, neo4j_file_url, task_id))
                            dic["errcode"] = 0
                    else:
                        dic["errcode"] = 0
                    if "ipv6" in command_info["action"]:
                        dic["url"] = web_host + "/ipv6_link?task="+task_id[5:]
                    else:
                        dic["url"] = web_host + "/ipv4_link?task="+task_id[5:]
                else:
                    dic["errcode"] = 1
                    dic["msg"] = "empty url or task_id"
            else:
                dic["errcode"] = 1
                dic["msg"] = "require url and task_id"
        else:
            dic["errcode"] = 1
            dic["msg"] = "param error"

        sock.sendall(bytes(json.dumps(dic)))
        sock.close()
        print "convert complete"

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "usage: python links_server.py <host> <port>"
        exit()
    host = sys.argv[1]
    port = sys.argv[2]
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, int(port)))
    s.listen(10)
    process = Processer()
    while True:
        sock, addr= s.accept()
        t=threading.Thread(target=process.startConvert,args=(sock, addr,));
        t.start()

