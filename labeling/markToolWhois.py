#coding: utf-8
import json, time, threading, os, MySQLdb, radix
import struct
import socket
import networkx as nx
import subprocess
import IPy
from networkx.algorithms import community

def ip2int(ipstr):
	return IPy.IP(ipstr).int()

def loadRTRInfo(rtr_info):
	print "load rtr info"
	rtr_dic = {}
	cnt = 1
	fp = open(rtr_info, "r")
	for eachline in fp:
		lineitem = eachline.strip().split(" ")
		for ip in lineitem:
			rtr_dic[ip] = cnt
		cnt += 1
	fp.close()
	print "load rtr info completed"
	return rtr_dic

def loadGEOInfo(geo_info):
	print "load geo info..."
	ip_info_list = []
	fp = open(geo_info, "r")
	for eachline in fp:
		lineitem = eachline.strip().split(",")
		lineitem[0] = int(lineitem[0])
		lineitem[1] = int(lineitem[1])
		ip_info_list.append(tuple(lineitem))
	fp.close()
	print "load geo info complete"
	return ip_info_list

# 建立BGP前缀树
def buildBGPPrefixTree(prefix2as):
	print "building BGP prefix tree..."
	rtree = radix.Radix()
	fp = open(prefix2as, "r")
	for eachline in fp:
		lineitem = eachline.strip().split(",")
		prefix = lineitem[0]
		asn = int(lineitem[1])
		rnode = rtree.add(prefix)
		rnode.data["asn"] = asn
	fp.close()
	print "building complete."
	return rtree

def buildWhoisTrie(prefix2whois):
	print "building whois trie ..."
	rtree = radix.Radix()
	fp = open(prefix2whois, "r")
	for eachline in fp:
		lineitem = eachline.strip().split(" ", 1)
		prefix = lineitem[0]
		whois = lineitem[1]
		rnode = rtree.add(prefix)
		rnode.data["whois"] = whois
	fp.close()
	print "building complete."
	return rtree

def getIPList(source, target, label, rtr_dic):

	print "get ip list"
	fp = open(source, "r")
	is_dest_dic = {}
	for eachline in fp:
		try:
			lineitem = eachline.strip().split(" ")
			is_dest = lineitem[2]
			if lineitem[0] not in is_dest:
				is_dest_dic[lineitem[0]] = "Y"
			if lineitem[1] not in is_dest:
				is_dest_dic[lineitem[1]] = is_dest
			elif is_dest == "N":
				is_dest_dic[lineitem[1]] = is_dest
		except:
			continue
	fp.close()
	outfp = open(target, "w")
	for ip in is_dest_dic:
		ip_num = ip2int(ip)
		if ip in rtr_dic:
			outfp.write("%s,%d,%s,%s,%s\n" % (ip, ip_num, is_dest_dic[ip], rtr_dic[ip], label))
		else:
			outfp.write("%s,%d,%s,0,%s\n" % (ip, ip_num, is_dest_dic[ip], label))
	outfp.close()

def formatLinks(source, target, label):
	print "format links..."
	fp = open(source, "r")
	outfp = open(target, "w")
	for eachline in fp:
		lineitem = eachline.strip().split(" ")
		if len(lineitem) < 10:
			continue
		outfp.write("%s,%s\n" % (",".join(lineitem), label))
	outfp.close()
	fp.close()
	print "format links complete"
# 导出数据库BGP信息
def exportBGPInfo(host, port, user, passwd, db, target):
	conn = MySQLdb.connect(host = host, port = port, user = user, passwd = passwd, db = db)
	cursor = conn.cursor()
	sql = "select prefix, origin from originss"
	cursor.execute(sql)
	result_list = cursor.fetchall()
	outfp = open(target, "w")
	for result in result_list:
		if ":" in result[0]:
			continue
		try:
			int(result[1])
			outfp.write("%s,%s\n" % result)
		except:
			continue
	outfp.close()


# 标记AS号
def markASNLabel(source, rtree, target):
	print "marking asn label..."
	start_time = time.time()
	fp  = open(source, "r")
	outfp = open(target, "w")
	for eachline in fp:
		lineitem = eachline.strip().split(",")
		ip = lineitem[0]
		rnode = rtree.search_best(ip)
		asn = 0
		if rnode != None:
			asn = rnode.data["asn"]
		asn = str(asn)
		outfp.write("%s,%s,%s\n" % (",".join(lineitem[:-1]), asn, lineitem[-1]))
	outfp.close()
	fp.close()
	end_time = time.time()
	print "marking asn label complete. cost time: " + str(end_time - start_time)


def exportGEOInfo(host, port, user, passwd, db, target):
	conn = MySQLdb.connect(host = host, port = port, user = user, passwd = passwd, db = db)
	cursor = conn.cursor()
	sql = "select ip_from, ip_to, country_name, region_name, city_name from ip2location_data"
	cursor.execute(sql)
	result_list = cursor.fetchall()
	index = 0
	outfp = open(target, "w")
	while index < len(result_list):
		result = list(result_list[index])
		if result[2]:
			result[2] = result[2].replace(',', '.')
		if result[3]:
			result[3] = result[3].replace(',', '.')
		if result[4]:
			result[4] = result[4].replace(',', '.')
		if result[0] > result[1]:
			print result
			if index + 1 < len(result_list):
				result[1] = result_list[index + 1][0] - 1
			else:
				result[1] = result[0]
		outfp.write("%d,%d,%s,%s,%s\n" % tuple(result))
		index += 1
	outfp.close()

def getInfoInGeoList(ip, ip_info_list):
	ip_num = ip2int(ip)
	start = 0
	end = len(ip_info_list) - 1
	while start <= end:
		mid = (start + end) / 2
		item = ip_info_list[mid]
		if ip_num < item[0]:
			end = mid - 1
		elif ip_num > item[1]:
			start = mid + 1
		else:
			return item[2:]
	return ('None', 'None', 'None')


def filtLinksByRtt(source, target):
	print "filt links by rtt"
	fp = open(source, "r")
	outfp = open(target, "w")
	count = 0
	out_count = 0
	for eachline in fp:
		count += 1
		lineitem = eachline.strip().split(" ")
		if len(lineitem) <= 5:
			continue
		delay = lineitem[4]
		if float(delay) > 0.5 or float(delay) == 0.0:
			continue
		src_ip = lineitem[0]
		dst_ip = lineitem[1]
		out_count += 1
		outfp.write("%s %s %s\n" % (src_ip, dst_ip, delay))
	print "%d lines, %d out lines" % (count, out_count)
	outfp.close()
	fp.close()
	print "filt links by rtt complete"

def splitLinkByGEO(source, ip_info_list, target):
	print "split link by geo"
	link_tuple_dic = {}
	fp = open(source, "r")
	for eachline in fp:
		lineitem = eachline.strip().split(" ")
		src_ip = lineitem[0]
		dst_ip = lineitem[1]
		delay = lineitem[2]
		src_geo_info = getInfoInGeoList(src_ip, ip_info_list)
		dst_geo_info = getInfoInGeoList(dst_ip, ip_info_list)
		if src_geo_info != dst_geo_info:
			continue
		format_geo = "%s->%s->%s" % src_geo_info
		if format_geo not in link_tuple_dic:
			link_tuple_dic[format_geo] = []
		link_tuple_dic[format_geo].append((src_ip, dst_ip, delay))
	fp.close()
	outfp = open(target, "w")
	for format_geo in link_tuple_dic:
		outfp.write("----"+format_geo + "----\n")
		for tuple_item in link_tuple_dic[format_geo]:
			outfp.write(" ".join(tuple_item) + "\n")
	outfp.close()

def splitEachCityLinksByCommunities(source, target):
	print "split each city links by communities"
	format_geo = ""
	link_delay_dic = {}
	fp = open(source, "r")
	outfp = open(target, "w")
	for eachline in fp:
		if "----" in eachline:
			if format_geo != "":
				result_communities = []
				lst = list(nx.connected_components(G))
				for item in lst:
					result_communities.append(list(item))
				outfp.write("----%s----\n" % format_geo)
				outfp.write(json.dumps(result_communities) + "\n")
			format_geo = eachline.strip()[4:-4]
			G = nx.Graph()
			continue
		lineitem = eachline.strip().split(" ")
		src_ip = lineitem[0]
		dst_ip = lineitem[1]
		delay = lineitem[2]
		if float(delay) > 0.5 or float(delay) == 0.0:
			continue
		link_delay_dic[(src_ip, dst_ip)] = float(delay)
		G.add_edge(src_ip, dst_ip)
	if format_geo != "":
		result_communities = []
		lst = list(nx.connected_components(G))
		for item in lst:
			result_communities.append(list(item))
		outfp.write("----%s----\n" % format_geo)
		outfp.write(json.dumps(result_communities) + "\n")
	fp.close()
	outfp.close()

def markPopNumber(pop_communities, pop_target):
	print "mark pop number"
	pop_counter = 1
	fp = open(pop_communities, "r")
	outfp = open(pop_target, "w")
	for eachline in fp:
		if "----" in eachline:
			continue
		communities = json.loads(eachline.strip())
		for group in communities:   
			for ip in group:
				outfp.write("%s,%d\n" % (ip, pop_counter))
			pop_counter += 1
	fp.close()
	outfp.close()

def markGEOLabel(source, ip_info_list, target):
	print "marking geo label..."
	start_time = time.time()
	fp  = open(source, "r")
	outfp = open(target, "w")
	for eachline in fp:
		lineitem = eachline.strip().split(",")
		ip = lineitem[0]
		info = getInfoInGeoList(ip, ip_info_list)
		outfp.write("%s,%s,%s\n" % (",".join(lineitem[:-1]), ",".join(info), lineitem[-1]))
	outfp.close()
	fp.close()
	end_time = time.time()
	print "marking geo label complete. cost time: " + str(end_time - start_time)

def markWhoisLabel(source, whois_trie, target):
	print "marking whois label..."
	start_time = time.time()
	fp  = open(source, "r")
	outfp = open(target, "w")
	for eachline in fp:
		lineitem = eachline.strip().split(",")
		ip = lineitem[0]
		rnode = whois_trie.search_best(ip)
		whois = '-'
		if rnode != None:
			whois = rnode.data["whois"]
		outfp.write("%s,%s,%s\n" % (",".join(lineitem[:-1]), whois, lineitem[-1]))
	outfp.close()
	fp.close()
	end_time = time.time()
	print "marking whois label complete. cost time: " + str(end_time - start_time)

def markDNSLabel(source, target):
	print "marking dummy dns label..."
	start_time = time.time()
	fp  = open(source, "r")
	outfp = open(target, "w")
	for eachline in fp:
		lineitem = eachline.strip().split(",")
		outfp.write("%s,null,%s\n" % (",".join(lineitem[:-1]), lineitem[-1]))
	outfp.close()
	fp.close()
	end_time = time.time()
	print "marking dummy dns label complete. cost time: " + str(end_time - start_time)

def importPoPInfo(source):
	print "import pop info"
	pop_dic = {}
	fp = open(source, "r")
	for eachline in fp:
		lineitem = eachline.strip().split(",")
		pop_dic[lineitem[0]] = lineitem[1]
	fp.close()
	print "import pop info complete"
	return pop_dic

def markPoPLabel(source, pop_dic, target):
	print "marking pop label"
	start_time = time.time()
	fp = open(source, "r")
	outfp = open(target, "w")
	for eachline in fp:
		lineitem = eachline.strip().split(",")
		pop_id = '0'
		ip = lineitem[0]
		if ip in pop_dic:
			pop_id = pop_dic[ip]
		outfp.write("%s,%s,%s\n" % (",".join(lineitem[:-1]), pop_id, lineitem[-1]))
	end_time = time.time()
	print "marking pop label complete. cost time: " + str(end_time - start_time)

def getNeo4jNode(source, target):
	print "getting neo4j nodes..."
	fp = open(source, "r")
	outfp = open(target, "w")
	for eachline in fp:
		lineitem = eachline.strip().split(",")
		outfp.write("%s,%s\n" % (lineitem[0], lineitem[-1]))
	outfp.close()
	fp.close()
	print "getting neo4j node complete"

def getNeo4jLink(source, target):
	print "getting neo4j links..."
	fp = open(source, "r")
	outfp = open(target, "w")
	for eachline in fp:
		lineitem = eachline.strip().split(",")
		outfp.write("%s,%s,%s\n" % (lineitem[0], lineitem[1], lineitem[-1]))
	outfp.close()
	fp.close()
	print "getting neo4j links complete"

def processLinksToLabel(source, rtr_dic, rtree, whois_trie, ip_info_list, target, node_label, link_label, osscan2node=''):
	dic = {}
	start_time = time.time()
	try:
		if not os.path.isdir(target):
			os.mkdir(target)
		getIPList(source, "%s/ip_list" % target, node_label, rtr_dic)
		getNeo4jNode("%s/ip_list" % target, "%s/neo4j-nodes.csv" % target)
		formatLinks(source, "%s/links.csv" % target, link_label)
		getNeo4jLink("%s/links.csv" % target, "%s/neo4j-links.csv" % target)
		markASNLabel("%s/ip_list" % target, rtree, "%s/ip_asn.csv" % target)
		filtLinksByRtt(source, "%s/pop_links" % target)
		splitLinkByGEO("%s/pop_links" % target, ip_info_list, "%s/pop_links_group" % target)
		splitEachCityLinksByCommunities("%s/pop_links_group" % target, "%s/pop_communities" % target)
		markPopNumber("%s/pop_communities" % target, "%s/ip_pop.csv" % target)
		pop_dic = importPoPInfo("%s/ip_pop.csv" % target)
		markPoPLabel("%s/ip_asn.csv" % target, pop_dic, "%s/ip_asn_pop.csv" % target)
		markGEOLabel("%s/ip_asn_pop.csv" % target, ip_info_list, "%s/ip_asn_pop_geo.csv" % target)
		markWhoisLabel("%s/ip_asn_pop_geo.csv" % target, whois_trie, "%s/ip_asn_pop_geo_whois.csv" % target)
		markDNSLabel("%s/ip_asn_pop_geo_whois.csv" % target, "%s/nodes.csv" % target)
		end_time = time.time()
		dic["errcode"] = 0
	except Exception,e:
		print e
		dic["errcode"] = 1
		dic["msg"] = "process links to label fail!"
	print "process links to label complete. cost time: " + str(end_time - start_time)
	return json.dumps(dic)

def clearOldLinksTask(source_dir, import_dir, task_id):
	print "clear old nodes and links in source and import directory"
	os.system("rm -rf %s" % (source_dir+"/"+task_id))
	os.system("rm -rf %s" % (import_dir+"/"+task_id))

def linksToDatabase(source_dir, import_dir, task_id):
	if not os.path.isdir(import_dir + "/" + task_id):
		os.mkdir(import_dir + "/" + task_id)
	print "link neo4j-nodes and links to import directory"
	os.system("ln -s %s %s" % (source_dir+"/"+task_id+"/neo4j-nodes.csv", import_dir+"/"+task_id+"/neo4j-nodes.csv"))
	os.system("ln -s %s %s" % (source_dir+"/"+task_id+"/neo4j-links.csv", import_dir+"/"+task_id+"/neo4j-links.csv"))

def loadPOPInfo(pop_info):
	dic = {}
	fp = open(pop_info, "r")
	for eachline in fp:
		lineitem = eachline.strip().split(",")
		dic[lineitem[0]] = lineitem[1]
	fp.close()
	return dic

def analysLinks(links_file, rtr_info, asn_info, pop_info, geo_info, statistic_file):
	dic = {}
	ip_appear_set = set()
	links_count = 0
	fp = open(links_file, "r")
	for eachline in fp:
		lineitem = eachline.strip().split(" ")
		if len(lineitem) < 9:
			continue
		links_count += 1
		ip_appear_set.add(lineitem[0])
		ip_appear_set.add(lineitem[1])
	fp.close()
	print "ip num: %d, links: %d" % (len(ip_appear_set), links_count)
	dic["ip_num"] = len(ip_appear_set)
	dic["links"] = links_count
	del ip_appear_set
	return 

	rtr_dic = loadRTRInfo(rtr_info)
	rtr_links = set()
	rtr_appear_set = set()
	fp = open(links_file, "r")
	for eachline in fp:
		lineitem = eachline.strip().split(" ")
		if len(lineitem) < 9:
			continue
		src_ip = lineitem[0]
		dst_ip = lineitem[1]
		if src_ip in rtr_dic:
			src_rtr = rtr_dic[src_ip]
			rtr_appear_set.add(src_rtr)
		if dst_ip in rtr_dic:
			dst_rtr = rtr_dic[dst_ip]
			rtr_appear_set.add(dst_rtr)
		if src_ip in rtr_dic and dst_ip in rtr_dic:
			src_rtr = rtr_dic[src_ip]
			dst_rtr = rtr_dic[dst_ip]
			if src_rtr == dst_rtr:
				continue
			rtr_links.add((src_rtr, dst_rtr))
	fp.close()
	print "rtr num: %d, rtr links: %d" % (len(rtr_appear_set), len(rtr_links))
	dic["rtr_num"] = len(rtr_appear_set)
	dic["rtr_links"] = len(rtr_links)
	del rtr_dic
	del rtr_links
	del rtr_appear_set

	rtree = buildBGPPrefixTree(asn_info)
	asn_links = set()
	asn_appear_set = set()
	fp = open(links_file, "r")
	for eachline in fp:
		lineitem = eachline.strip().split(" ")
		if len(lineitem) < 9:
			continue
		src_ip = lineitem[0]
		dst_ip = lineitem[1]
		src_rnode = rtree.search_best(src_ip)
		src_asn = 0
		if src_rnode != None:
			src_asn = src_rnode.data["asn"]
			asn_appear_set.add(src_asn)
		dst_rnode = rtree.search_best(dst_ip)
		dst_asn = 0
		if dst_rnode != None:
			dst_asn = dst_rnode.data["asn"]
			asn_appear_set.add(dst_asn)
		if src_asn != 0 and dst_asn != 0:
			if src_asn == dst_asn:
				continue
			asn_links.add((src_asn, dst_asn))
	fp.close()
	print "asn num: %d, asn links: %d" % (len(asn_appear_set), len(asn_links))
	dic["asn_num"] = len(asn_appear_set)
	dic["asn_links"] = len(asn_links)
	del rtree
	del asn_links
	del asn_appear_set

	pop_dic = loadPOPInfo(pop_info)
	pop_links = set()
	pop_appear_set = set()
	fp = open(links_file, "r")
	for eachline in fp:
		lineitem = eachline.strip().split(" ")
		if len(lineitem) < 9:
			continue
		src_ip = lineitem[0]
		dst_ip = lineitem[1]
		if src_ip in pop_dic:
			src_pop = pop_dic[src_ip]
			pop_appear_set.add(src_pop)
		if dst_ip in pop_dic:
			dst_pop = pop_dic[dst_ip]
			pop_appear_set.add(dst_pop)
		if src_ip in pop_dic and dst_ip in pop_dic:
			src_pop = pop_dic[src_ip]
			dst_pop = pop_dic[dst_ip]
			if src_pop == dst_pop:
				continue
			pop_links.add((src_pop, dst_pop))
	fp.close()
	print "pop num: %d, pop links: %d" % (len(pop_appear_set), len(pop_links))
	dic["pop_num"] = len(pop_appear_set)
	dic["pop_links"] = len(pop_links)
	del pop_dic
	del pop_links
	del pop_appear_set

	ip_info_list = loadGEOInfo(geo_info)
	country_links = set()
	country_appear_set = set()
	city_links = set()
	city_appear_set = set()
	fp = open(links_file, "r")
	for eachline in fp:
		lineitem = eachline.strip().split(" ")
		if len(lineitem) < 9:
			continue
		src_ip = lineitem[0]
		dst_ip = lineitem[1]
		src_geo = getInfoInGeoList(src_ip, ip_info_list)
		dst_geo = getInfoInGeoList(dst_ip, ip_info_list)
		country_appear_set.add(src_geo[0])
		country_appear_set.add(dst_geo[0])
		city_appear_set.add(src_geo[2])
		city_appear_set.add(dst_geo[2])
		if src_geo[0] != 'None' and dst_geo[0] != 'None' and src_geo[0] != dst_geo[0]:
			country_links.add((src_geo[0], dst_geo[0]))
		if src_geo[2] != 'None' and dst_geo[2] != 'None' and src_geo[2] != dst_geo[2]:
			city_links.add((src_geo[2], dst_geo[2]))
	fp.close()
	print "country num: %d, country links: %d" % (len(country_appear_set), len(country_links))
	dic["country_num"] = len(country_appear_set)
	dic["country_links"] = len(country_links)
	print "city num: %d, city links: %d" % (len(city_appear_set), len(city_links))
	dic["city_num"] = len(city_appear_set)
	dic["city_links"] = len(city_links)
	outfp = open(statistic_file, "w")
	json.dump(dic, outfp, indent = 4)
	outfp.close()


# if __name__ == "__main__":
	# rtr_dic = loadRTRInfo("rtr.info")
	# rtree = buildBGPPrefixTree("prefix2as.csv")
	# ip_info_list = loadGEOInfo("iprange2geo.csv")
	# task_id = "2018-12-06"
	# processLinksToLabel("2018-12-06.links", rtr_dic, rtree, ip_info_list, task_id, "node", "edge")
	# analysLinks("201808.merged.links", "rtr.info", "prefix2as.csv", "201808/ip_pop.csv", "iprange2geo.csv", "statistic.js")