import sys
import json
import socket
import struct
import math

def ip2int(ip):
  packedIP = socket.inet_aton(ip)
  return struct.unpack("!L", packedIP)[0]

def int2ip(i):
  return socket.inet_ntoa(struct.pack('!L',i))

def r2c_helper(a, b, l, h):
  if (a,b) == (l,h):
    return [(l,h)]
  m = (h+l)/2
  if b <= m:
    return r2c_helper(a, b, l, m)
  elif a >= m+1:
    return r2c_helper(a, b, m+1, h)
  else:
    return r2c_helper(a, m, l, m) + r2c_helper(m+1, b, m+1, h)

def range2cidr(a, b):
  l, h = 0, 2**32-1
  return map( lambda x: int2ip(x[0])+'/'+str(32-int(math.log(x[1]-x[0]+1, 2))), r2c_helper(a, b, l, h) )

while True:
  try:
    l = raw_input().strip()
  except:
    break
  a, b, g = l.split('|', 2); a = a.replace(' ',''); b = b.replace(' ','')
  if '0.0.0.0' in [a,b]:
    continue
  try:
    a, b = ip2int(a), ip2int(b)
  except:
    pass
    continue
  for r in range2cidr(a, b):
    print r, g
