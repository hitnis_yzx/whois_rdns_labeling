import sys

if len(sys.argv) < 2:
  exit()

fn = sys.argv[1]
f = open(fn)

while True:
  l = f.readline()
  if not l:
    break
  sys.stdout.write(l)
