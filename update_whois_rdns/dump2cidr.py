import patricia

trie = patricia.BinPatriciaTrie()
while True:
  try:
    l = raw_input().strip()
  except:
    break
  pfx, whois = l.split(' ',1)
  if pfx == '0.0.0.0/0':
    continue
  trie.add_string(pfx, whois)
patricia.pre(trie)
