import sys

if len(sys.argv) < 2:
  exit()

attr_l = sys.argv[1].split(',')
# attr_l = ['inetnum', 'netname', 'country', 'descr']
# attr_l = ['NetRange', 'OrgID', 'NetName']

obj = {}
while True:
  try:
    l = raw_input().strip()
  except:
    break
  # ignore comments
  if l and l[0] == '#':
    continue
  # output previous object
  if not l:
    if obj and all(map(lambda a: a in obj, attr_l)) == True and not 'V6NetHandle' in obj:
      print '|'.join([obj[a] for a in attr_l])
    obj = {}
    continue

  # example:
  #   inetnum:        202.6.91.0 - 202.6.91.255
  #   netname:        NLA
  #   country:        AU
  #   descr:          National Library of Australia
  f = l.split(None, 1)
  if len(f) < 2:
    continue
  k, v = f[0], f[1]; k = k[:-1]
  obj[k] = v if not k in obj else obj[k]+','+v

if obj and all(map(lambda a: a in obj, attr_l)) == True and not 'V6NetHandle' in obj:
  print '|'.join([obj[a] for a in attr_l])
