import sys
import re
import json
import math
import socket
import struct

def ip2int(ip):
  packedIP = socket.inet_aton(ip)
  return struct.unpack("!L", packedIP)[0]

def int2ip(i):
  return socket.inet_ntoa(struct.pack('!L',i))

class BinPatriciaTrie():
  def __init__(self):
    self.root = {}

  @staticmethod
  def l(ptr):
    if '0' in ptr:
      return ptr['0']
    return None

  @staticmethod
  def r(ptr):
    if '1' in ptr:
      return ptr['1']
    return None

  def is_not_valid(self, v):
    pattern_l = ['managed by', '.*allocated to.*', '.*allocated by.*']
    for p in pattern_l:
      if re.match(p, v):
        return True
    return False


  def add_string(self, s, v):
    p, m = s.split('/'); m = int(m)
    ptr = self.root
    for b in bin(ip2int(p))[2:].zfill(32)[:m]:
      if not b in ptr:
        ptr[b] = {}
      ptr = ptr[b]
    flag = 'v'
    if flag in ptr or self.is_not_valid(v):
      return
    ptr[flag] = v

  def match_string(self, s):
    ptr = self.root
    sl = []; pl = []; path = ""
    # !important: add placeholder '#', otherwise /32 won't be matched
    for i,b in enumerate(bin(ip2int(s))[2:].zfill(32)+'#'):
      if 'm' in ptr:
        sl.append(ptr['m'])
        # pl.append(' '.join([ int2ip(int(path.ljust(32,'0'), 2)) + '/' + str(len(path)), 'non-leaf', path, ptr['m'] ]))
      if 'v' in ptr:
        sl.append(ptr['v'])
        # pl.append(' '.join([ int2ip(int(path.ljust(32,'0'), 2)) + '/' + str(len(path)), 'leaf', path, ptr['v'] ]))
      if not b in ptr:
        break
      ptr = ptr[b]
      path += b
    # return pl, (None if not sl else sl[-1])
    return None if not sl else sl[-1]

def pre_helper(ptr, path, v):
  l,r = BinPatriciaTrie.l(ptr), BinPatriciaTrie.r(ptr)
  if 'v' in ptr:
    v = ptr['v']

  if v and not l and not r:
    print ' '.join([ "%s/%d" % (int2ip(int(path.ljust(32,'0'), 2)), len(path)) ,v ])
    return
  if l:
    pre_helper(l, path+'0', v)
  elif v:
    print ' '.join([ "%s/%d" % (int2ip(int((path+'0').ljust(32,'0'), 2)), len(path+'0')) , v ])

  if r:
    pre_helper(r, path+'1', v)
  elif v:
    print ' '.join([ "%s/%d" % (int2ip(int((path+'1').ljust(32,'0'), 2)), len(path+'1')) , v ])

def pre(trie):
  ptr = trie.root
  if not ptr:
    return

  return pre_helper(ptr, '', None)
def r2c_helper(a, b, l, h):
  if (a,b) == (l,h):
    return [(l,h)]
  m = (h+l)/2
  if b <= m:
    return r2c_helper(a, b, l, m)
  elif a >= m+1:
    return r2c_helper(a, b, m+1, h)
  else:
    return r2c_helper(a, m, l, m) + r2c_helper(m+1, b, m+1, h)

def range2cidr(a, b):
  l, h = 0, 2**32-1
  return map( lambda x: int2ip(x[0])+'/'+str(32-int(math.log(x[1]-x[0]+1, 2))), r2c_helper(a, b, l, h) )
